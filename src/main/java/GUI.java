import javax.swing.*;
import javax.swing.border.LineBorder;
import javax.swing.filechooser.FileSystemView;
import java.awt.*;
import java.awt.event.*;
import java.util.regex.Pattern;

public class GUI implements ActionListener {

    public static JLabel label;
    public static JTextField text;
    public static JLabel label2;
    public static JTextField text2;
    public static JLabel label3;
    public static JTextField text3;
    public static JLabel label4;
    public static JTextField text4;
    public static JLabel label5;
    public static JTextField text5;
    public static JPasswordField passwordText;
    public static JButton button;
    public static JCheckBox configBox;
    public static JLabel configFileLabel;
    public static JButton configFileButton;
    
    public static void runGUI() {

        JFrame frame = new JFrame("404 Finder");
        JPanel panel = new JPanel();
        frame.setSize(430, 500);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        

        frame.add(panel);
        panel.setLayout(null);

        label = new JLabel("Seed URL:");
        label.setBounds(30, 100, 100, 25);
        panel.add(label);

        text = new JTextField(20);
        text.setBounds(150, 100, 200, 25);
        panel.add(text);

        label2 = new JLabel("Domain/URL Filter:");
        label2.setBounds(30, 135, 100, 25);
        panel.add(label2);

        text2 = new JTextField(20);
        text2.setBounds(150, 135, 200, 25);
        panel.add(text2);

        label3 = new JLabel("Crawl Depth:");
        label3.setBounds(30, 170, 100, 25);
        panel.add(label3);

        text3 = new JTextField(20);
        text3.setBounds(150, 170, 200, 25);
        panel.add(text3);

        label4 = new JLabel("Max Pages:");
        label4.setBounds(30, 205, 100, 25);
        panel.add(label4);

        text4 = new JTextField(20);
        text4.setBounds(150, 205, 200, 25);
        panel.add(text4);

        label5 = new JLabel("Number of Threads:");
        label5.setBounds(30, 240, 120, 25);
        panel.add(label5);

        text5 = new JTextField(20);
        text5.setBounds(150, 240, 200, 25);
        panel.add(text5);

        configFileButton = new JButton("Open");
        configFileButton.setBounds(30, 350, 80, 25);
        configFileButton.addActionListener(new GUI());
        panel.add(configFileButton);

        configBox = new JCheckBox("Read From Config File", false);
        configBox.setBounds(27, 300, 200, 25);
        panel.add(configBox);

        button = new JButton("Run");
        button.setBounds(270, 350, 80, 25);
        button.addActionListener(new GUI());
        panel.add(button);

        configFileLabel = new JLabel("Select Config File");
        configFileLabel.setBounds(30, 322, 200, 25);
        panel.add(configFileLabel);

        Font font1 = new Font("SansSerif", Font.BOLD, 48);
        JLabel title  = new JLabel("404 Finder");
        title.setBounds(85, 23, 300, 50);
        title.setFont(font1);
        panel.add(title);

        Font font2 = new Font("SansSerif", Font.PLAIN, 12);
        JLabel stitle  = new JLabel("Jefferson Van Buskirk - jfv222@lehigh.edu");
        stitle.setBounds(10, 430, 300, 15);
        stitle.setFont(font2);
        panel.add(stitle);
        
        JLabel info  = new JLabel("Powered by crawler4j");
        info.setBounds(10, 445, 300, 15);
        info.setFont(font2);
        panel.add(info);


        frame.setVisible(true);
    }

@Override
public void actionPerformed(ActionEvent e) {
    String com = e.getActionCommand(); 

    //if user pressed run button
	if (com.equals("Run")) { 
        //if config is selected read from there
        if (configBox.isSelected()) {
            //if config file is selected
            Globals.setGlobalsFromConfig();
            App.runCrawler();
        }
        //if gui read from gui
        else {
            if (setGlobalsFromGUI())
                App.runCrawler();
        }

    }
        // if the user presses the open dialog show the open dialog 
		else { 
			// create an object of JFileChooser class 
			JFileChooser j = new JFileChooser(FileSystemView.getFileSystemView().getHomeDirectory()); 
			// invoke the showsOpenDialog function to show the save dialog 
			int r = j.showOpenDialog(null); 

			// if the user selects a file 
			if (r == JFileChooser.APPROVE_OPTION){
				// set the label to the path of the selected file 
                configFileLabel.setText(j.getSelectedFile().getAbsolutePath());  
                Globals.CONFIG_FILE_PATH = j.getSelectedFile().getAbsolutePath();
            }
			// if the user cancelled the operation 
			else
				configFileLabel.setText("Select Config File"); 
		} 
    }

    //sets the global variables based on the inputs in the gui
    public static boolean setGlobalsFromGUI () {
        boolean bool = true;
        Globals.SEED_URL = text.getText();
        Globals.DOMAIN = text2.getText();
        Globals.DOMAIN_FILTER = Pattern.compile(".*" + Globals.DOMAIN + ".*"); 
        try {
        Globals.MAX_CRAWL_DEPTH = Integer.parseInt(text3.getText());
        }
        catch (NumberFormatException e) {
            text3.setBorder(new LineBorder(Color.RED,2));
            bool = false;
        }
        try {
            Globals.MAX_PAGES = Integer.parseInt(text4.getText());
            }
        catch (NumberFormatException e) {
            text4.setBorder(new LineBorder(Color.RED,2));
            bool = false;
            }
        try {
            Globals.NUMBER_OF_CRAWLERS = Integer.parseInt(text5.getText());
        }
        catch (NumberFormatException e) {
            text5.setBorder(new LineBorder(Color.RED,2));
            bool = false;
            }
        return bool;
    }
}