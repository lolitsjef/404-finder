import java.util.*;
import edu.uci.ics.crawler4j.url.WebURL;

public class DeadLink {
    public String wURL;
    public int statusCode;
    public String statusDescription;
    public String parentURL;
    public String linkText;
}
