import java.util.*;
import java.io.*;
import java.util.regex.Pattern;

import javax.swing.plaf.BorderUIResource.LineBorderUIResource;

public class Globals {
    public static ArrayList<DeadLink> deadlinks = new ArrayList<DeadLink>();
    public static String CONFIG_FILE_PATH = "config.txt";                   //passed as string args[]?
    public static String OUTPUT_FILE_PATH = "./output/404output.txt";
    public static int MAX_CRAWL_DEPTH = 2;
    public static int NUMBER_OF_CRAWLERS = 5;
    public static String CRAWL_STORAGE = "/data/crawl/root";
    public static String SEED_URL;
    public static String DOMAIN;
    public static Pattern DOMAIN_FILTER;
    public static int MAX_PAGES;
    public static int delay = 10;

    //writes the globals to the output file
    public static void writeGlobals(){
        try {
            FileWriter myWriter = new FileWriter(Globals.OUTPUT_FILE_PATH, true);
            myWriter.write("Seed URL: " + SEED_URL + "\n");
            myWriter.write("Domain: " + DOMAIN + "\n");
            myWriter.write("Crawl Depth: " + MAX_CRAWL_DEPTH + "\n");
            myWriter.write("Max Pages: " + MAX_PAGES + "\n");
            myWriter.write("\n");

            myWriter.close();
        }
        catch (Exception e) {
            System.out.println("Error writing globals to file");
        }
    }
    
    //read from config file  
    public static void setGlobalsFromConfig() {
        try {
            String line = "";
            BufferedReader buffRead = new BufferedReader(new FileReader((CONFIG_FILE_PATH)));

            String varName;
            while ((line = buffRead.readLine()) != null){
                Scanner charlie = new Scanner(line);
                charlie.useDelimiter(" = ");
                varName = charlie.next();

                if (varName.equals("OUTPUT_FILE_PATH"))
                    OUTPUT_FILE_PATH = charlie.next();
                else if (varName.equals("OUTPUT_FILE_NAME"))
                    OUTPUT_FILE_PATH = charlie.next();
                else if (varName.equals("MAX_CRAWL_DEPTH"))
                    MAX_CRAWL_DEPTH = charlie.nextInt();
                else if (varName.equals("NUMBER_OF_CRAWLERS"))
                    NUMBER_OF_CRAWLERS = charlie.nextInt();
                else if (varName.equals("MAX_PAGES"))
                    MAX_PAGES = charlie.nextInt();
                else if (varName.equals("SEED_URL"))
                    SEED_URL = charlie.next();
                else if (varName.equals("DOMAIN"))
                    DOMAIN = charlie.next();
                else if (varName.equals("DELAY"))
                    delay = charlie.nextInt();
                else System.out.println("NO MATCH");
                charlie.close();
            }
            buffRead.close();
            DOMAIN_FILTER = Pattern.compile(".*" + DOMAIN + ".*"); 
        }
        catch (Exception e) {
            System.out.println("Error in config file");
        }
        
    }
}
