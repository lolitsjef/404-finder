public class Main {
    public static void main(String [] args) throws Exception{
        
        if(args.length == 1){
            Globals.CONFIG_FILE_PATH = args[0];
            Globals.setGlobalsFromConfig();
            App.runCrawler();
            HTML_Maker.writeHTML();
            CSV_Maker csv = new CSV_Maker();
            csv.writeCSV();
            System.exit(0);
        }
        else if (args.length > 1){
            App.help();
        }
        GUI.runGUI();
        
    }
}
