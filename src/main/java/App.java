import edu.uci.ics.crawler4j.crawler.*;
import edu.uci.ics.crawler4j.fetcher.PageFetcher;
import edu.uci.ics.crawler4j.robotstxt.RobotstxtConfig;
import edu.uci.ics.crawler4j.robotstxt.RobotstxtServer;
import edu.uci.ics.crawler4j.*;
import java.util.logging.*;
import java.io.*;

import java.util.*;


public class App {
  public static void runCrawler () {
    try {
        try {
            File myFile = new File(Globals.OUTPUT_FILE_PATH);
            myFile.delete();
            myFile.createNewFile();
          } 
        catch (IOException e) {
            System.out.println("An error occurred making the output file.");
            e.printStackTrace();
          }


        
        //Instantiate crawl config
         
        CrawlConfig config = new CrawlConfig();
        config.setCrawlStorageFolder(Globals.CRAWL_STORAGE);
        config.setMaxDepthOfCrawling(Globals.MAX_CRAWL_DEPTH);
        config.setPolitenessDelay(Globals.delay);
        if (Globals.MAX_PAGES > 0)
          config.setMaxPagesToFetch(Globals.MAX_PAGES);
        
        
        //Instantiate controller for this crawl.
         
        PageFetcher pageFetcher = new PageFetcher(config);
        RobotstxtConfig robotstxtConfig = new RobotstxtConfig();
        RobotstxtServer robotstxtServer = new RobotstxtServer(robotstxtConfig, pageFetcher);
        CrawlController controller = new CrawlController(config, pageFetcher, robotstxtServer);


        controller.addSeed(Globals.SEED_URL);     
        Globals.writeGlobals();
        System.out.println("Starting the Crawl, this may take a while..");
        controller.start(TestCrawler.class, Globals.NUMBER_OF_CRAWLERS);
      }
      catch (Exception E){
        System.out.println("Please check to make sure the config input is fomatted correctly");
        E.printStackTrace();
      }
  }


  public static void help(){
    System.out.println("Run with one command, that command being the config file name or path.");
  }
}