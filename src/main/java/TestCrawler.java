import edu.uci.ics.crawler4j.crawler.*;
import edu.uci.ics.crawler4j.parser.HtmlParseData;
import edu.uci.ics.crawler4j.url.WebURL;

import java.io.*;

import java.util.Set;
import java.util.regex.Pattern;

import org.apache.http.HttpStatus;
import org.apache.http.impl.EnglishReasonPhraseCatalog;
import org.slf4j.LoggerFactory;

public class TestCrawler extends WebCrawler{

    private final static Pattern FILTERS = Pattern.compile(".*(\\.(css|js|gif|jpg|png|mp3|mp4|pdf|zip|gz))$");

    //check if the url should be crawled
    @Override
    public boolean shouldVisit(Page referringPage, WebURL url) {
        //System.out.println("shouldVisit: " + url.getURL().toLowerCase());
        String href = url.getURL().toLowerCase();
        boolean result = !FILTERS.matcher(href).matches();
        result = result && Globals.DOMAIN_FILTER.matcher(href).matches();       

        return result;
    }

    //function executed when crawling a page
    @Override
    public void visit(Page page) {
        String url = page.getWebURL().getURL();
        System.out.println("Crawling URL: " + url);


        if (page.getParseData() instanceof HtmlParseData) {
            HtmlParseData htmlParseData = (HtmlParseData) page.getParseData();      
            String text = htmlParseData.getText(); //extract text from page
            String html = htmlParseData.getHtml(); //extract html from page
            Set<WebURL> links = htmlParseData.getOutgoingUrls();
            /*
            System.out.println("---------------------------------------------------------");
            System.out.println("Parent URL: " + page.getWebURL().getParentUrl());
            System.out.println("Page URL: " + url);
            System.out.println("Text length: " + text.length());
            System.out.println("Html length: " + html.length());
            System.out.println("Number of outgoing links: " + links.size());
            System.out.println("---------------------------------------------------------");  
            */
        }
    }

     //checks if the return code is a 404, if so it logs it in the file
    @Override
    protected void handlePageStatusCode (WebURL webUrl, int statusCode, String statusDescription) {
        if (statusCode == 404 && !webUrl.getAnchor().equals("null") && !webUrl.getAnchor().equals("(HTML)") && !webUrl.getAnchor().equals("HTML")){ 
            
            System.out.println("---------------------------------------------------------");  
            System.out.println("404 URL: " + webUrl);
            System.out.println("---------------------------------------------------------");  
            
           synchronized(this){
           DeadLink i = new DeadLink();
           i.wURL = webUrl.toString();
           i.linkText = webUrl.getAnchor();
           i.parentURL = webUrl.getParentUrl();
           i.statusCode = statusCode;
    
           Globals.deadlinks.add(i);
           }
           

            synchronized(this){
                try {
                    FileWriter myWriter = new FileWriter(Globals.OUTPUT_FILE_PATH, true);
                    myWriter.write("Parent URL: " + webUrl.getParentUrl() + "\n");
                    myWriter.write("Link Text: " + webUrl.getAnchor() + "\n");
                    myWriter.write("Page URL: " + webUrl  + "\n");
                    myWriter.write("Error Code: " + statusCode  + "\n");
                    //myWriter.write("Status Description: " + statusDescription  + "\n");
                    myWriter.write("\n");
                    myWriter.close();
                }
                catch (Exception e){
                    System.out.println("An error occurred wirting to the output file.");
                }
            } 
        }
    }
}

