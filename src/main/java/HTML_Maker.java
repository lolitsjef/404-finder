import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class HTML_Maker {

    public static void writeTopHTML(){

        try {
            FileWriter myWriter = new FileWriter("index.html", true);
            myWriter.write("<html lang=\"en\">" + "\n");
            myWriter.write("<head>" + "\n");
            myWriter.write("<meta charset=\"utf-8\">" + "\n");
            myWriter.write("" + "\n");
            myWriter.write("<title>404 Finder Results</title>" + "\n");
            myWriter.write("<meta name=\"description\" content=\"404 Finder Results\">" + "\n");
            myWriter.write("<meta name=\"author\" content=\"Jefferson Van Buskirk\">" + "\n");
            myWriter.write("" + "\n");
            myWriter.write("" + "\n");
            myWriter.write("</head>" + "\n");
            myWriter.write("" + "\n");
            myWriter.write("<body>" + "\n");
            myWriter.write("<h1 align= 'center'>404 Finder Results</h1>" + "\n");
            myWriter.write("<h4 align= 'center'>Jefferson Van Buskirk</h4>" + "\n");
            myWriter.close();
        }
        catch (Exception e) {
            System.out.println("Error writing to html file");
        }
    }
    public static void writeBottomHTML(){

        try {
            FileWriter myWriter = new FileWriter("index.html", true);
            myWriter.write("</body>" + "\n");
            myWriter.write("</html>" + "\n");

            myWriter.close();
        }
        catch (Exception e) {
            System.out.println("Error writing to html file");
        }
    }
    public static void createHTML(){
        try {
            File myFile = new File("./output/index.html");
            myFile.delete();
            myFile.createNewFile();
          } 
        catch (IOException e) {
            System.out.println("An error occurred making the html file.");
            e.printStackTrace();
          }
    }
    public static void writeGlobals(){
        try {
            FileWriter myWriter = new FileWriter("./output/index.html", true);
            myWriter.write("<br><span>Crawl Settings</span>" + "\n");
            myWriter.write("<br><span>Seed URL: </span> <a href=\"" + Globals.SEED_URL + "\">https://www.lehigh.edu</a>" + "\n");
            myWriter.write("<br><span>Domain: </span> <span>" + Globals.DOMAIN + "</span>" + "\n");
            myWriter.write("<br><span>Crawl Depth: </span> <span>" + Globals.MAX_CRAWL_DEPTH + "</span>" + "\n");
            if (Globals.MAX_PAGES > 0)
                myWriter.write("<br><span>Max Pages: </span> <span>" + Globals.MAX_PAGES + "</span>" + "\n");
             
            myWriter.close();
        }
        catch (Exception e) {
            System.out.println("Error writing to html file");
        }

        
    }
    public static void writeContent(){
        
        String oldParent = "";
        System.out.println(Globals.deadlinks.size());
        for (int i = 0; i < Globals.deadlinks.size(); i++) {
            DeadLink currLink = Globals.deadlinks.get(i);

            //if the link has the same parent as the last one
            if (currLink.parentURL.equals(oldParent)){
                try {
                    FileWriter myWriter = new FileWriter("./output/index.html", true);
                    myWriter.write("<br>"+ "\n");
                    myWriter.write("<br><span style=\"font-weight:normal\">&nbsp;&nbsp;&nbsp;&nbsp;Dead URL: </spanp> <a href=\"" + currLink.wURL + "\">" + currLink.wURL + "</a>"+ "\n");
                    myWriter.write("<br><span style=\"font-weight:normal\">&nbsp;&nbsp;&nbsp;&nbsp;Link Text: " + currLink.linkText + "</span>"+ "\n");
                    myWriter.close();
                }
                catch (Exception e) {
                    System.out.println("Error writing to html file");
                }
            }
            //if a new parent url
            else {
                //update old parent
                oldParent = currLink.parentURL;
                try {
                    FileWriter myWriter = new FileWriter("./output/index.html", true);
                    myWriter.write("<br>"+ "\n");
                    myWriter.write("<br>"+ "\n");
                    myWriter.write("<br><span style=\"font-weight:bold\">Parent URL: </spanp> <a href=\"" + currLink.parentURL + "\">" + currLink.parentURL + "</a>"+ "\n");
                    myWriter.write("<br><span style=\"font-weight:normal\">&nbsp;&nbsp;&nbsp;&nbsp;Dead URL: </spanp> <a href=\"" + currLink.wURL + "\">" + currLink.wURL + "</a>"+ "\n");
                    myWriter.write("<br><span style=\"font-weight:normal\">&nbsp;&nbsp;&nbsp;&nbsp;Link Text: " + currLink.linkText + "</span>"+ "\n");
                    myWriter.close();
                }
                catch (Exception e) {
                    System.out.println("Error writing to html file");
                }
            }
        }


    }
    public static void writeHTML(){
        createHTML();
        writeTopHTML();
        writeGlobals();
        writeContent();
        writeBottomHTML();
    }
}
