import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;


public class CSV_Maker {


    public void writeGlobals(){
    }
    public void writeContent(){
        
        //create list of lines of data for the csv
        List<String[]> dataLines = new ArrayList<>();
        for (DeadLink i : Globals.deadlinks){
            dataLines.add(new String[] { i.parentURL, i.wURL, i.linkText,});
        }
        File csvOutputFile = new File("./output/404Output.csv");
        try (PrintWriter pw = new PrintWriter(csvOutputFile)) {
            dataLines.stream()
              .map(this::convertToCSV)
              .forEach(pw::println);
        }
        catch (Exception e) {
            System.out.println("Error Making CSV file");
        }
    }
   
    /* Code inspired by https://www.baeldung.com/java-csv */
    public String convertToCSV(String[] data) {
        return Stream.of(data)
          .map(this::escapeSpecialCharacters)
          .collect(Collectors.joining(","));
    }
    public String escapeSpecialCharacters(String data) {
        String escapedData = data.replaceAll("\\R", " ");
        if (data.contains(",") || data.contains("\"") || data.contains("'")) {
            data = data.replace("\"", "\"\"");
            escapedData = "\"" + data + "\"";
        }
        return escapedData;
    }

    public void writeCSV(){
        //writeGlobals();
        writeContent();
    }
}


