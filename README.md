404 Finder
Jefferson Van Buskirk

1. How to run 404 finder

    To run with GUI:   
    java -jar target/404_Finder-0.1.0 

    To run from a config file:   
    java -jar target/404_Finder-0.1.0 config.txt

2. Settings

    OUTPUT_FILE_PATH = Output file to write 404 errors to
    MAX_CRAWL_DEPTH = How many links deep it will go from the seed URL
    NUMBER_OF_CRAWLERS = Amount of threads
    SEED_URL = URL that is crawled first
    DOMAIN = keyword that is used as a filter for every url that is checked, if the url doesn't containt the keyword then it will not be crawled
    MAX_PAGES = Maximum pages to crawl
    DELAY = How fast the crawler should run (some servers will kick you off if it crawls too fast)

3. Example config file

    OUTPUT_FILE_PATH = LEHIGH_404.txt
    MAX_CRAWL_DEPTH = 5
    NUMBER_OF_CRAWLERS = 20
    SEED_URL = https://www.lehigh.edu/home
    DOMAIN = lehigh.edu
    DELAY = 5
    MAX_PAGES = 100

4. Output

    The 404 Finder will produce a .txt file that shows all of the links found. It will also produce an index.html file  that can be opened to view the output with clickable links.
